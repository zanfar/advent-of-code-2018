#!/usr/bin/env python3

import sys
import logging
import argparse
import json
import os
import os.path
import re

__program__ = 'day5'
__description__ = 'Advent of Code 2018: Day 5 solution'
__version__ = '0.1.0'
__email__ = 'me@mattwyant.com'

def wd():
    """Return the working directory"""
    return os.getcwd()

def ld():
    """Return the local directory (where the script is located)"""
    return os.path.dirname(os.path.realpath(__file__))

def get_cli_opts():
	"""Parse sys.argv for command line options
	returns: namespace of option values"""

	parser = argparse.ArgumentParser(
			prog=__program__,
			description=__description__,
			epilog='Version '+__version__+', created and maintained by '+__email__,
			add_help=False,
			fromfile_prefix_chars='@'
			)

	parser.add_argument(
			'INPUT',
			action='store', default=None,
			help='Input file to run against')

	parser.add_argument(
			'-h', '--help',
			action='help',
			help='show this help message and exit')
	parser.add_argument(
			'-v', '--verbose',
			action='count', default=0,
			help='Increase logging verbosity')
	parser.add_argument(
			'-V', '--version',
			action='version', version="{:s} {:s}".format(__program__, __version__),
			help='Print version information')
	parser.add_argument(
			'--logfile',
			action='store',
			help='Logfile to use')

	opts = parser.parse_args()
	return opts

def setup_logging(verbosity=0, logfile=None):
	"""Setup a root logger based on a verbosity count and logfile path

	verbosity (int): Which log messages to display: 0-3, 0 being errors only
			and 3 being all log messages; default 0
	logfile (str): Path to write log messages to (in addition to stdout), if None
			disables the feature; default None
	"""

	root_logger = logging.getLogger()
	root_logger.setLevel(logging.DEBUG)
	logger = logging.getLogger(__name__)

	console_handler = logging.StreamHandler()
	console_handler.setLevel(logging.CRITICAL)
	console_handler.setFormatter(logging.Formatter('[%(levelname)s] %(name)s: %(message)s'))

	if not verbosity or verbosity == 0:
		console_handler.setLevel('ERROR')
	elif verbosity == 1:
		console_handler.setLevel('WARNING')
	elif verbosity == 2:
		console_handler.setLevel('INFO')
	elif verbosity >= 3:
		console_handler.setLevel('DEBUG')
	else:
		logger.critical('Unexplained negative count while setting handler verbosity')

	root_logger.addHandler(console_handler)

	if (logfile is not None):
		logfile = "{:s}.{:s}.log".format(logfile, datetime.date.today().strftime('%Y%m%d'))
		file_handler = logging.FileHandler(logfile)
		file_handler.setLevel(logging.DEBUG)
		file_handler.setFormatter(logging.Formatter('%(asctime)s [%(levelname)s] %(name)s: %(message)s'))
		root_logger.addHandler(file_handler)
		logger.debug("Logging to file '{:s}'".format(logfile))

def read_sequence_from_file(infile_path):
	logger = logging.getLogger(__name__)
	with open(infile_path, 'r') as fh:
		return ''.join([ line.strip() for line in fh ])

def is_reactive(a, b):
	logger = logging.getLogger(__name__)
	return a != b and (a.lower() == b or a.upper() == b)

def reduce_sequence(sequence):
	logger = logging.getLogger(__name__)

	changed = True
	reacted = 0
	reduced = sequence

	while changed:
		changed = False

		for x in reversed(range(len(reduced)-1)):
			if x >= len(reduced)-1:
				continue

			logger.debug("Checking pair {:s}".format(reduced[x:x+2]))
			if is_reactive(reduced[x], reduced[x+1]):
				logger.debug("Found reactive pair in polymer at position {:d}".format(x))
				reacted += 1

				if x == 0:
					reduced = reduced[x+2:]
				elif x == len(reduced)-2:
					reduced = reduced[:x]
				else:
					reduced = reduced[:x] + reduced[x+2:]

	return reduced

def unique_units(sequence):
	for unit in set([u for u in sequence.lower()]):
		yield unit

def remove_unit(sequence, unit):
	return sequence.replace(unit.lower(), '').replace(unit.upper(), '')

def main():
	"""Main program execution"""
	opts = get_cli_opts()
	setup_logging(opts.verbose, opts.logfile)

	logger = logging.getLogger(__name__)
	logger.debug("Running with options: {:s}".format(json.dumps(vars(opts))))

	seq = read_sequence_from_file(opts.INPUT)
	logger.info("Input polymer is {:d} units long".format(len(seq)))

	reduced = reduce_sequence(seq)
	logger.info("Reduced polymer is {:d} units long".format(len(reduced)))
	print(len(reduced))
	reduced = None

	min_length = len(seq)
	for unit in unique_units(seq):
		logger.debug("Checking for removal of unit {:s}".format(unit))
		removed = remove_unit(seq, unit)
		logger.debug("Removed sequence: {:s}".format(removed))
		reduced = reduce_sequence(removed)
		logger.info("Removing unit {:s} results in a reduction to {:d} units".format(unit, len(reduced)))

		if len(reduced) < min_length:
			min_length = len(reduced)
	print(min_length)

	return 0

if __name__ == "__main__":
	"""direct execution entry point, handoff to main()"""
	sys.exit(main())

